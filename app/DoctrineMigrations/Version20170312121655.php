<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170312121655 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE attribute (id INTEGER NOT NULL, name CLOB NOT NULL, weight INTEGER NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FA7AEFFB5E237E06 ON attribute (name)');
        $this->addSql('CREATE TABLE hotel (id INTEGER NOT NULL, name CLOB NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3535ED95E237E06 ON hotel (name)');
        $this->addSql('CREATE TABLE remark (id INTEGER NOT NULL, name CLOB NOT NULL, weight INTEGER NOT NULL, changer BOOLEAN NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E1CAD8395E237E06 ON remark (name)');
        $this->addSql('CREATE TABLE review (id INTEGER NOT NULL, id_hotel INTEGER NOT NULL, text CLOB NOT NULL, score INTEGER NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE topic (id INTEGER NOT NULL, name CLOB NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9D40DE1B5E237E06 ON topic (name)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE attribute');
        $this->addSql('DROP TABLE hotel');
        $this->addSql('DROP TABLE remark');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE topic');
    }
}
