<?php

namespace SatisfactionMeterBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;


/**
 * ReviewRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ReviewRepository extends EntityRepository
{
    public function getAllReviews()
    {
        return $this->createQueryBuilder('r')
            ->add('select', 'h.id as idHotel, h.name as hotel, r.id, r.text, r.createdAt, r.score')
            ->leftJoin('SatisfactionMeterBundle:Hotel', 'h')
            ->where('h.id = r.id_hotel')
            ->orderBy('r.id', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function getReviewById($id)
    {
        return $this->createQueryBuilder('r')
            ->add('select', 'h.id as idHotel, h.name as hotel, r.id, r.text, r.createdAt, r.score')
            ->leftJoin('SatisfactionMeterBundle:Hotel', 'h', 'WITH', 'h.id = r.idHotel')
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();
    }

    //Get reviews applying the filters that we pass onto it, used to fill the jQuery Grid
    public function getReviews($gridParams, $count = false)
    {
        $qb = $this->createQueryBuilder('r');
        if ($count) {
            $qb->add('select', 'COUNT(r.id)');
        }
        else {
            $qb->add('select', 'h.id as idHotel, h.name as hotel, r.id, r.text, r.createdAt, r.score');
        }
        $qb->join('SatisfactionMeterBundle:Hotel', 'h', Join::WITH, 'h.id = r.id_hotel');

        $qb ->add('where', $qb->expr()->orX(
            $qb->expr()->eq('r.id', ':search'),
            $qb->expr()->like('h.name', ':search'),
            $qb->expr()->like('r.text', ':search'),
            $qb->expr()->like('r.score', ':search')
        ));

        if (isset($gridParams['sortBy']) && (isset($gridParams['sortOrder'])))
        {
            switch ($gridParams['sortBy']) {
                case 'id':
                    $orderBy = "r.id";
                    break;
                case 'hotel':
                    $orderBy = "h.name";
                    break;
                case 'text':
                    $orderBy = "r.text";
                    break;
                case 'score':
                    $orderBy = "r.score";
                    break;
                default:
                    $orderBy = "r.id";
            }
            $qb->orderBy($orderBy, $gridParams['sortOrder']);
        }

        if(false == $count) {
            $qb->setFirstResult($gridParams['current'] * $gridParams['rowCount'] - $gridParams['rowCount'])
                ->setMaxResults($gridParams['rowCount']);
        }

        $qb->setParameters(array(
            'search' => "%".$gridParams['searchPhrase']."%",
            )
        );

        $qb = $qb->getQuery();
        if ($count) {
            $qb = $qb->getSingleScalarResult();
        }
        else {
            $qb = $qb->getArrayResult();
        }

        return $qb;
    }
}
