<?php

namespace SatisfactionMeterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use SatisfactionMeterBundle\Entity\Hotel;


class ReviewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id_hotel', EntityType::class, array(
                'class' => 'SatisfactionMeterBundle:Hotel',
                'choice_label' => 'name'
            ))
            ->add('text')
        ;
    }

    public function getBlockPrefix() {
        return "reviewForm";
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SatisfactionMeterBundle\Entity\Review',
        ));
    }
}