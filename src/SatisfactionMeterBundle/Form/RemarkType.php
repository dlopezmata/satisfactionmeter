<?php

namespace SatisfactionMeterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;



class RemarkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('weight',ChoiceType::class, array(
                'choices'  => array(
                    '1' => 'Just a bit',
                    '2' => 'A lot',
                )))
            ->add('changer', ChoiceType::class, array(
                'choices' => array('No' => false, 'Yes' => true),
                'choices_as_values' => true,
            ));
        ;
    }

    public function getBlockPrefix() {
        return "remarkForm";
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SatisfactionMeterBundle\Entity\Remark',
        ));
    }
}