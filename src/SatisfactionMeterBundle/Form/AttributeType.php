<?php

namespace SatisfactionMeterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;



class AttributeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('weight', ChoiceType::class, array(
                'choices'  => array(
                    '1' => 'Possitive',
                    '2' => 'Very possitive',
                    '-1' => 'Negative',
                    '-2' => 'Very negative'
                )))
        ;
    }

    public function getBlockPrefix() {
        return "attributeForm";
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SatisfactionMeterBundle\Entity\Attribute',
        ));
    }
}