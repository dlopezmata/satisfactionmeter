<?php

namespace SatisfactionMeterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    //This controller is not used at all
    /**
     * @Route("/", name="home")
     */
    public function indexAction()
    {

        echo "This is home";
    }
}
