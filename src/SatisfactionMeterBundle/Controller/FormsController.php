<?php

namespace SatisfactionMeterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\File;
use SatisfactionMeterBundle\Form\AttributeType;
use SatisfactionMeterBundle\Entity\Attribute;
use SatisfactionMeterBundle\Form\ReviewType;
use SatisfactionMeterBundle\Entity\Review;
use SatisfactionMeterBundle\Form\TopicType;
use SatisfactionMeterBundle\Entity\Topic;
use SatisfactionMeterBundle\Form\HotelType;
use SatisfactionMeterBundle\Entity\Hotel;
use SatisfactionMeterBundle\Form\RemarkType;
use SatisfactionMeterBundle\Entity\Remark;
use SatisfactionMeterBundle\Repository\TopicRepository;
use SatisfactionMeterBundle\Repository\AttributeRepository;
use SatisfactionMeterBundle\Repository\RemarkRepository;


class FormsController extends Controller
{
    //This action is called to print Add Entity Form
    public function addEntityAction(request $request, $type)
    {
        $entity = $this->createEntity($type);
        $form = $this->addFormCreation($type, $entity);

        return $this->render('@SatisfactionMeterBundle/Resources/views/Forms/add' . $type . '.html.twig', array(
            'form_' . $type => $form->createView(),
        ));
    }

    //This action is called to send the Add Entity form via Ajax
    /**
     * @Route("/proccessEntity", name="proccessEntity")
     * @Method("POST")
     */
    public function processEntityAction (request $request)
    {
        $type = $request->query->get('type');
        $entity = $this->createEntity($type);
        $form = $this->addFormCreation($type, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $textAnalysis = $this->get('text_analysis');
            $em = $this->getDoctrine()->getManager();
            if ($type == 'review') {
                $entity->setIdHotel($entity->getIdHotel()->getId());
                $entity->setScore($textAnalysis->getScoreNewReview($entity));
            }
            else {
                $entity->setName(strtolower($entity->getName()));
            }
            $em->persist($entity);
            $em->flush();

            //We recalculate the scores in case our new criteria affects them
            $textAnalysis->recalculateScores();

            return new JsonResponse(array(
                'type' => $type,
                'message' => 'Indicated '.$type.' has been correctly inserted!',
                'form' => $this->renderView('@SatisfactionMeterBundle/Resources/views/Forms/add'.$type.'.html.twig',
                    array(
                        'entity' => $type,
                        'form_'.$type => $form->createView(),
                    ))), 200);

        }
        $response = new JsonResponse(
            array(
                'type' => $type,
                'message' => (string) $form->getErrors(true, false),
                'form' => $this->renderView('@SatisfactionMeterBundle/Resources/views/Forms/add'.$type.'.html.twig',
                    array(
                        'entity' => $type,
                        'form_'.$type => $form->createView(),
                    ))), 400);

        return $response;
    }

    //This is called to show the remove entities form and is called everytime we add a new entity to be able to show it
    /**
     * @Route("/removeEntity/{typeajax}", name="removeEntity")
     * @Method("POST")
     */
    public function removeEntityAction(request $request, $type = false, $typeajax = false)
    {
        if (false != $typeajax) {
            $type = $typeajax;
        }

        $entity = $this->createEntity($type);
        $form = $this->removeFormCreation($type, $entity);

        return $this->render('@SatisfactionMeterBundle/Resources/views/Forms/remove' . $type . '.html.twig', array(
            'form_remove_' . $type => $form->createView(),
        ));
    }

    //This is called when we submit the remove entities form, via Ajax
    /**
     * @Route("/proccessRemoveEntity", name="proccessRemoveEntity")
     * @Method("POST")
     */
    public function ProccessRemoveEntityAction (request $request)
    {

        $type = $request->query->get('type');
        $entity = $this->createEntity($type);
        $form = $this->removeFormCreation($type, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $textAnalysis = $this->get('text_analysis');

            $toDeleteArray = $request->request->get('form');
            if (isset($toDeleteArray['name'])) {
                foreach ($toDeleteArray['name'] as $id) {
                    $entity = $em->getRepository('SatisfactionMeterBundle:' . $type)->find($id);
                    if (!$entity) {
                        throw $this->createNotFoundException('No entity found');
                    }
                    $em->remove($entity);
                    $em->flush();

                }
                //When we delete criteria we calculate all the scores again
                $textAnalysis->recalculateScores();

                return new JsonResponse(array(
                    'type' => $type,
                    'message' => 'Selected '.$type . '/s deleted correctly!',
                    'form' => $this->renderView('@SatisfactionMeterBundle/Resources/views/Forms/remove' . $type . '.html.twig',
                        array(
                            'entity' => $type,
                            'form_remove_' . $type => $form->createView(),
                        ))), 200);
            }

        }

        $response = new JsonResponse(
            array(
                'type' => $type,
                'message' => 'Please, select some '.$type. " to delete",
                'form' => $this->renderView('@SatisfactionMeterBundle/Resources/views/Forms/remove'.$type.'.html.twig',
                    array(
                        'entity' => $type,
                        'form_remove_'.$type => $form->createView(),
                    ))), 400);

        return $response;
    }

    /**
     * @Route("/uploadCsv", name="uploadCsv")
     */
    public function uploadCsvAction(request $request)
    {
        $form = $this->uploadCsvFormCreation();

        return $this->render('@SatisfactionMeterBundle/Resources/views/Forms/addcsv.html.twig', array(
            'form_csv' => $form->createView(),
        ));
    }

    //Called when uploading CSV via Ajax
    /**
     * @Route("/proccessUploadCsv", name="proccessUploadCsv")
     */
    public function proccessUploadCsvAction(request $request)
    {
        $form = $this->uploadCsvFormCreation();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $type = $form['type']->getData();
            $file = $form['file']->getData();
            $extension = $file->guessExtension();

            if ($extension == "csv" || $extension == "txt") {

                $contents = file($file->getPathname());
                $contents = array_filter( $contents );

                $textAnalysis = $this->get('text_analysis');

                $results = $textAnalysis->insertCsv($contents, $type);

                $success = 200;
                $message = $results['inserted'] . " new " . $type . "/s have been inserted.";
                if (1 <= $results['exists']) {
                    $message .= " ".$results['exists'] . " " . $type ."/s already existed.";
                }
                if ("" != $results['extra']) {
                    $message .= $results['extra'];
                }
                if (true == $results['errors']) {
                    $message .= " There were some errors on the CSV file.";
                    $success = 400;
                }

                return new JsonResponse(array(
                    'type' => $type,
                    'message' => $message,
                ), $success);
            }
        }

    }

    private function createEntity($type)
    {
        switch ($type) {
            case 'topic':
                $entity = new Topic();
                break;
            case 'attribute':
                $entity = new Attribute();
                break;
            case 'remark':
                $entity = new Remark();
                break;
            case 'review':
                $entity = new Review();
                break;
            default:
                break;
        }
        return $entity;
    }

    private function addFormCreation($type, $entity)
    {
        switch ($type) {
            case 'topic':
                $form = $this->createForm(TopicType::class, $entity, array(
                        'action' => $this->generateUrl('proccessEntity', array('type' => $type)),
                    )
                );
                break;
            case 'attribute':
                $form = $this->createForm(AttributeType::class, $entity, array(
                        'action' => $this->generateUrl('proccessEntity', array('type' => $type)),
                    )
                );
                break;
            case 'remark':
                $form = $this->createForm(RemarkType::class, $entity, array(
                        'action' => $this->generateUrl('proccessEntity', array('type' => $type)),
                    )
                );
                break;
            case 'review':
                $form = $this->createForm(ReviewType::class, $entity, array(
                        'action' => $this->generateUrl('proccessEntity', array('type' => $type)),
                    )
                );
                break;
            default:
                break;
        }
        return $form;
    }

    private function removeFormCreation($type, $entity)
    {
        switch ($type) {

            case 'topic':
                $form = $this->createFormBuilder($entity)
                    ->add('name', EntityType::class, array(
                        'class' => 'SatisfactionMeterBundle:Topic',
                        'choice_label' => 'name',
                        'multiple' => true,
                        'expanded' => true,
                        'query_builder' => function (TopicRepository $er) {
                            return $er->createQueryBuilder('t')
                                ->orderBy('t.name', 'ASC');
                        }
                    ))
                    ->getForm();
                break;

            case 'attribute':
                $form = $this->createFormBuilder($entity)
                    ->add('name', EntityType::class, array(
                        'class' => 'SatisfactionMeterBundle:Attribute',
                        'choice_label' => 'name',
                        'multiple' => true,
                        'expanded' => true,
                        'query_builder' => function (AttributeRepository $er) {
                            return $er->createQueryBuilder('a')
                                ->orderBy('a.name', 'ASC');
                        }
                    ))
                    ->getForm();
                break;

            case 'remark':
                $form = $this->createFormBuilder($entity)
                    ->add('name', EntityType::class, array(
                        'class' => 'SatisfactionMeterBundle:Remark',
                        'choice_label' => 'name',
                        'multiple' => true,
                        'expanded' => true,
                        'query_builder' => function (RemarkRepository $er) {
                            return $er->createQueryBuilder('r')
                                ->orderBy('r.name', 'ASC');
                        }
                    ))
                    ->getForm();
                break;
        }
        return $form;
    }

    private function uploadCsvFormCreation()
    {
        $form = $this->createFormBuilder()
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Reviews' => 'review',
                    'Topics' => 'topic',
                    'Attributes' => 'attribute',
                    'Remarks' => 'remark',
                ),
                'choices_as_values' => true
                ))
            ->add('file', FileType::class, array(
                'required' => 'true',
                'constraints' => [
                    new File([
                        'mimeTypes' => [
                            'application/csv',
                            'application/x-csv',
                            'text/plain',
                            'text/csv',
                            'text/comma-separated-values',
                            'text/x-comma-separated-values',
                            'text/tab-separated-values',
                            'application/vnd.ms-excel'
                        ],
                        'mimeTypesMessage' => 'Upload a valid CSV',
                    ])
                ]
            ))
            ->getForm();

        return $form;
    }

}



