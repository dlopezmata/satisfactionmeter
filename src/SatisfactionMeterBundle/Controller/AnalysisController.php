<?php

namespace SatisfactionMeterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use SatisfactionMeterBundle\Form\HotelType;
use SatisfactionMeterBundle\Entity\Hotel;
use SatisfactionMeterBundle\Entity\Review;


class AnalysisController extends Controller
{
    /**
     * @Route("/", name="analysis")
     */
    public function indexAction(Request $request)
    {
        $hotel = new Hotel();

        $formHotel = $this->createForm(HotelType::class, $hotel, array(
            )
        );

        if ($request->request->has('hotelForm')) {
            $formHotel->handleRequest($request);
            if ($formHotel->get('hotel_save')->isClicked()) {
                if ($formHotel->isSubmitted() && $formHotel->isValid()) {

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($hotel);
                    $em->flush();

                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'Hotel ' . $formHotel->get('name')->getData() . ' has been added.'
                    );
                }
            }
        }

        return $this->render('@SatisfactionMeterBundle/Resources/views/index.html.twig', array(
            'form_hotel' => $formHotel->createView(),
        ));
    }

    /**
     * @Route("/getReviewsJson", name="getJson")
     */
    public function getReviewsJson()
    {
        $textAnalysis = $this->get('text_analysis');

        return ($textAnalysis->prepareReviewsJson());
    }

    /**
     * @Route("/removeReview/{id}", name="removeReview")
     * @Method("POST")
     */
    public function removeReview($id)
    {
        $em = $this->getDoctrine()->getManager();
        $review = $em->getRepository('SatisfactionMeterBundle:Review')->find($id);
        if (!$review) {
            throw $this->createNotFoundException('No entity found');
        }
        $em->remove($review);
        $em->flush();

        return new JsonResponse(array(
            'message' => 'Selected review has been deleted correctly!'
            ), 200);
    }
}


