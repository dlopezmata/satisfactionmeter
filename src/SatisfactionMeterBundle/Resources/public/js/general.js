$(document).ready(function () {
    $("#new-review-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#f-review").slideDown();
    });

    $("#new-hotel-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#hotelForm").slideDown();
    });

    $("#new-topic-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#f-topic").slideDown();
        $("#r-topic").slideDown();
    });

    $("#new-attribute-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#f-attribute").slideDown();
        $("#r-attribute").slideDown();
    });

    $("#new-remark-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#f-remark").slideDown();
        $("#r-remark").slideDown();
    });

    $("#csv-btn").click(function() {
        $(".form").slideUp().delay( 300 );
        $("#f-csv").slideDown();
    });

    setTimeout(function() {
        $('.alert').slideUp('slow');
    }, 8000);

    var hotel = $("#reviewForm_id_hotel").val();
    if (!!hotel){
        $('#new-review-btn').removeAttr('disabled');
    }
    else {
        $('#hotel-alert').html("Please, create a Hotel to be able to add reviews for it.");
    }
});