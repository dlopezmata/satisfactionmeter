function initAjaxForm()
{
    $('body').on('submit', '.ajaxForm', function (e) {

        e.preventDefault();

        //We do this to being able to append a file for the Ajax call, used at importing CSV
        fdata = new FormData($(this)[0]);

        if($("#form_file")[0].files.length>0)
        {
            fdata.append("file", $("#form_file")[0].files[0]);
        }

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: fdata,
            processData: false,
            contentType: false
        })
            .done(function (data) {
                if (typeof data.message !== 'undefined') {

                    var type = data.type;
                    if (type != 'review') {
                        $.ajax({
                            url: '/removeEntity/' + data.type,
                            type: 'POST',
                            success: function (data, status) {
                                switch (type) {
                                    case 'topic':
                                        $('#r-topic').html(data);
                                        break;
                                    case 'attribute':
                                        $('#r-attribute').html(data);
                                        break;
                                    case 'remark':
                                        $('#r-remark').html(data);
                                        break;
                                }

                            },
                            error: function () {
                                alert("error");
                            }
                        });
                    }
                    else {
                        $('#f-review').html(data.form);
                    }

                    $('#form_success').html(data.message);
                    $('#form_error').fadeOut(1000).delay(5000);;
                    $('#form_success').fadeIn(1000);
                    $('#grid-data').bootgrid('reload');
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                if (typeof jqXHR.responseJSON !== 'undefined') {
                    $('#form_error').html(jqXHR.responseJSON.message);
                    $('#form_success').fadeOut(1000).delay(5000);
                    $('#form_error').fadeIn(1000);


                } else {
                    alert(errorThrown);
                }

            });
    });

}