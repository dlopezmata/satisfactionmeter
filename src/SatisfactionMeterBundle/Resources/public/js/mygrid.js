$(document).ready(function () {
    var grid = $("#grid-data").bootgrid({
        labels: {
            loading: "Loading, please wait..."
        },
        ajax: true,
        post: function ()
        {
            /* To accumulate custom parameter with the request object */
            return {
                id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
            };
        },
        url: "/getReviewsJson",
        formatters: {
            "commands": function(column, row)
            {
                return "<button type=\"button\" class=\"btn btn-xs btn-danger remove-btn command-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon glyphicon-trash\"></span> </button>";
}
        }
    }).on("loaded.rs.jquery.bootgrid", function () {
        grid.find(".command-delete").on("click", function (e) {
            var id = $(this).data("row-id");

            $.ajax({
                url: '/removeReview/'+id,
                type: 'POST',
                success: function(data, status){
                    $('#form_success').html(data.message);
                    $('#form_error').fadeOut(1000).delay(1000);
                    $('#form_success').fadeIn(1000);
                    $('#grid-data').bootgrid('reload');
                },
                error: function () {
                    alert("error");
                }
            });




        })
    })   ;
});

