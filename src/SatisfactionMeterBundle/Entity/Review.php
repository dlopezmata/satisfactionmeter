<?php

namespace SatisfactionMeterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\Entity(repositoryClass="SatisfactionMeterBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
    *  @ORM\Column(name="id_hotel", type="integer")
     */
    private $id_hotel;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", length=65535, nullable=false, unique=false)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 2000,
     *      minMessage = "The review must contain at least {{ limit }} characters",
     *      maxMessage = "The review name name cannot be longer than {{ limit }} characters"
     * )
     */
    private $text;

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;


    public function __construct(){
        $this->setCreatedAt(new \DateTime());
    }

    /**
 * Set text
 *
 * @param string $text
 * @return Review
 */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set score
     *
     * @param string $score
     * @return Review
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Review
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id_hotel
     *
     * @param integer id_hotel
     * @return Review
     */
    public function setIdHotel($id_hotel)
    {
        $this->id_hotel = $id_hotel;

        return $this;
    }

    /**
     * Get id_hotel
     *
     * @return integer
     */
    public function getIdHotel()
    {
        return $this->id_hotel;
    }

}
