<?php

namespace SatisfactionMeterBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Attribute
 *
 * @ORM\Table(name="remark")
 * @UniqueEntity(
 *     fields={"name"},
 *     message="This remark already exists"
 * )
 * @ORM\Entity(repositoryClass="SatisfactionMeterBundle\Repository\RemarkRepository")
 */
class Remark
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", length=65535, nullable=false, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(
     *      min = 2,
     *      max = 150,
     *      minMessage = "The remark name must contain at least {{ limit }} characters",
     *      maxMessage = "The remark name name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer")
     * @Assert\NotBlank()
     *
     */
    private $weight;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changer", type="boolean")
     *
     */
    private $changer;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    public function __construct(){
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Remark
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Remark
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @return Remark
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set changer
     *
     * @return Remark
     */
    public function setChanger($changer)
    {
        $this->changer = $changer;

        return $this;
    }

    /**
     * Get changer
     *
     * @return integer
     */
    public function getChanger()
    {
        return $this->changer;
    }



}
