<?php

namespace SatisfactionMeterBundle\Service;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use SatisfactionMeterBundle\Entity\Hotel;
use SatisfactionMeterBundle\Entity\Review;
use SatisfactionMeterBundle\Entity\Topic;
use SatisfactionMeterBundle\Entity\Attribute;
use SatisfactionMeterBundle\Entity\Remark;

Class TextAnalysisService extends Controller
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    //Returns the reviews passed to it, adding a new array, "analysis", that contains the analysis of each encountered criteria and its score.
    public function analyse($reviews)
    {
        $criteria = $this->extractCriteria();

        $critExpression = array (
            'topics' => $this->createExpressionPart($criteria['topicPool']),
            'attributes' => $this->createExpressionPart($criteria['attributePool']),
            'remarks' => $this->createExpressionPart($criteria['remarkPool']),
        );


        /*Basically, what this expression looks is for a (required) Attribute that can be surrounded by Topics or Remarks as well as random text or other Attribute.
        Each Topic, Remark, or Attribute must be a complete word, for instance, "The" doesn't match the Topic "He".
        Basically:
        Topic + (mandatory)Attribute OR Remark + Topic + (mandatory)Attribute */
        $match = "/((((((\\s|^)(".$critExpression['topics'].")(s|ies)*)+(\\s)+(([a-zA-Z1-9']{0,})(\\s))*((".$critExpression['attributes'].")(\\s|$)+)+)|(((\\s|^)(".$critExpression['remarks']."))*((\\s|^)(".$critExpression['attributes'].")(\\s|$)+)+(((([a-zA-Z1-9']{0,})(\\s))*)+(((".$critExpression['topics'].")(s|ies)*)(\\s|$)+)+)*))((\\s|$)+)*)+)/i";

        $processedReviews = $reviews;

        foreach($reviews as $index => $review) {
            $processedReviews[$index]['analysis'] = $this->analyzeReview($review['text'], $criteria, $match);
            $processedReviews[$index]['criteriaString'] = $this->foundToString($processedReviews[$index]['analysis']);

            //Here we check the score for the review, as the Grid will show the current one (not the one stored at the DB)
            $processedReviews[$index]['score'] = $this->currentScore($processedReviews[$index]['analysis']);
        }

        return $processedReviews;
    }

    //Returns a json compatible with the jQuery Bootgrid, taking into account, the filters that were passed.
    public function prepareReviewsJson()
    {
        $request = Request::createFromGlobals();

        $gridParams = array(
            'current' => $request->request->get('current'),
            'rowCount' => $request->request->get('rowCount'),
            'searchPhrase' => $request->request->get('searchPhrase'),
        );

        if (null != ($request->request->get('sort'))) {
            $sortkeys = (array_keys($request->request->get('sort')));
            $gridParams['sortBy'] = $sortkeys[0];
            $gridParams['sortOrder'] = $request->request->get('sort')[$sortkeys[0]];
        }

        $response = new \stdClass();
        $response->current = $gridParams['current'];
        $response->rowCount = $gridParams['rowCount'];

        $response->rows = array();
        $reviews = $this->em->getRepository('SatisfactionMeterBundle:Review')->getReviews($gridParams);
        $response->rows = $this->analyse($reviews);

        //"true" param is used to return the count
        $response->total = $this->em->getRepository('SatisfactionMeterBundle:Review')->getReviews($gridParams, true);

        return new Response(json_encode($response));
    }

    //Returns the calculated score for a new review, after preparing it to be correctly analysed as expected for analyse function (it needs an array).
    public function getScoreNewReview($review)
    {
        $arrayReview = array();
        $arrayReview[0] = array('text' => $review->getText());

        $analysed = $this->analyse($arrayReview);
        return $analysed[0]['score'];
    }

    //Recalculates the score for all reviews, this happens after inserting new criteria that would affect the score
    public function recalculateScores()
    {
        $reviews = $this->em->getRepository('SatisfactionMeterBundle:Review')->getAllReviews();
        $analysed = $this->analyse($reviews);
        foreach ($analysed as $review) {

            $entityReview = $this->em->getRepository('SatisfactionMeterBundle:Review')->find($review['id']);
            $entityReview->setScore($review['score']);
            $this->em->persist($entityReview);
            $this->em->flush();
        }
    }

    //Inserts CSV contents into DB and returns an array with the results of the insertion
    public function insertCsv($contents, $type)
    {
        $results = array(
            'inserted' => 0,
            'errors' => false,
            'exists' => 0,
            'extra' => "",
        );

        try {
            //We start on the second row of the CSV.
            foreach (array_slice($contents, 1) as $content) {
                $insert = true;
                $arrayContents = explode("|", $content);

                switch ($type) {
                    case 'review':

                        $entity = new Review();
                        $entity->setText(trim($arrayContents[1]));
                        $entity->setScore($this->getScoreNewReview($entity));

                        //We look for the hotel ID, we can only insert the review if the hotel exists.
                        $hotel = $this->em->getRepository('SatisfactionMeterBundle:Hotel')->getIdByName($arrayContents[0]);

                        if (isset($hotel[0]['id'])) {
                            $entity->setIdHotel($hotel[0]['id']);
                        } else {
                            $results['extra'] .= " Hotel '" . $arrayContents[0] . "' doesn't exist.";
                            $insert = false;
                        }
                        break;

                    case 'topic':

                        $topic = $this->em->getRepository('SatisfactionMeterBundle:Topic')->getIdByName(strtolower($arrayContents[0]));

                        if (isset($topic[0]['id'])) {
                            $results['exists']++;
                            $insert = false;
                        } else {
                            $entity = new Topic();
                            $entity->setName(trim(strtolower($arrayContents[0])));
                        }
                        break;

                    case 'attribute':

                        $attribute = $this->em->getRepository('SatisfactionMeterBundle:Attribute')->getIdByName(strtolower($arrayContents[0]));

                        if (isset($attribute[0]['id'])) {
                            $results['exists']++;
                            $insert = false;
                        } else {
                            $entity = new Attribute();
                            $entity->setName(trim(strtolower($arrayContents[0])));
                            if (is_numeric(trim($arrayContents[1]))) {
                                $entity->setWeight(trim($arrayContents[1]));
                            }
                        }
                        break;

                    case 'remark':

                        $remark = $this->em->getRepository('SatisfactionMeterBundle:Remark')->getIdByName(strtolower($arrayContents[0]));

                        if (isset($remark[0]['id'])) {
                            $results['exists']++;
                            $insert = false;
                        } else {
                            $entity = new Remark();
                            $entity->setName(trim(strtolower($arrayContents[0])));
                            if (is_numeric(trim($arrayContents[1]))) {
                                $entity->setWeight(trim($arrayContents[1]));
                                $entity->setChanger(trim($arrayContents[2]));
                            }
                        }
                        break;

                    default:
                        break;
                }

                if (true == $insert) {
                    $this->em->persist($entity);
                    $this->em->flush();
                    $results['inserted']++;
                }
            }
        }

        catch (\Exception $e){
            $results['errors'] = true;
        }

        return $results;
    }

    //Analyze a single review, returning an object that contains each encountered criteria and its score.
    private function analyzeReview($review, $criteria, $match) {

        $result = array();

        //We separate each line of the review
        $reviewLines =  preg_split('/[\.\,\;\?()-:!]+/', $review);

        foreach ($reviewLines as $line) {
            $line = preg_replace("/[^a-zA-Z0-9\\s&']/", "", $line);

            if(true == preg_match_all($match, $line, $encounters)) {

                foreach ($encounters[0] as $encounter) {
                    $encounterPoints = 0;

                    foreach ($criteria['attributePool'] as $attribute) {

                        if (substr_count(strtolower($encounter), strtolower($attribute['name'])) > 0)  {

                            $encounterPoints = $encounterPoints + ($attribute['weight'] * substr_count(strtolower($encounter), strtolower($attribute['name'])));

                            //This is used to find inside each encounter, the part that is used to calculate the encounter points.
                            $remarkExpression = $this->createExpressionPart($criteria['remarkPool']);
                            $attributeMatch = "/(((\\s|^)(".$attribute['name'].")(\\s)((".$remarkExpression.")(\\s|$))+)+|((\\s|^)((".$remarkExpression.")(\\s))+(".$attribute['name'].")(\\s|$))+)/i";

                            if(true == preg_match_all($attributeMatch, $encounter, $evaluableEncounter)) {

                                foreach ($evaluableEncounter[0] as $evaluable) {
                                    /*This is used to turn around the words so we can calculate the points of every word in the correct order,
                                    This is because in English, the meaning of the "attribute" or "remark" changes with the word at its left.
                                    So we need to calculate "Not really good" starting from "good", followed by "really" and then "Not".*/
                                    $evaluable = array_map('strrev', explode(' ', strrev($evaluable)));

                                    foreach ($evaluable as $word) {

                                        foreach ($criteria['remarkPool'] as $remark) {

                                            if ((strtolower($word) == strtolower($remark['name']))) {
                                                /*If the remark is a "changer", for instance "not", then we will turn the points to negative in case the actual points of
                                                this evaluable part was possitive. Else, we add the weight of the remark to the actual points*/
                                                if (true == $remark['changer']) {
                                                    if ($encounterPoints >=0) $encounterPoints = $encounterPoints * (-$remark['weight']);

                                                    else $encounterPoints++;

                                                } else {
                                                    if ($attribute['weight'] > 0) {
                                                        $encounterPoints = $encounterPoints + $remark['weight'];
                                                    }
                                                    else {
                                                        $encounterPoints = $encounterPoints - $remark['weight'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //We only add the line score if it got some score.
                    if (0 != $encounterPoints) {
                        array_push($result, ['line' => trim($encounter), 'points' => $encounterPoints]);
                    }
                }
            }
        }

        return $result;
    }

    //Extracts criteria from the database.
    private function extractCriteria()
    {
        $topicPool = $this->em->getRepository('SatisfactionMeterBundle:Topic')->getAllTopics();

        $attributePool = $this->em->getRepository('SatisfactionMeterBundle:Attribute')->getAllAttributes();

        $remarkPool = $this->em->getRepository('SatisfactionMeterBundle:Remark')->getAllRemarks();

        $criteria = array(
          'topicPool' => $topicPool,
          'attributePool' => $attributePool,
          'remarkPool' => $remarkPool,
        );

        return $criteria;
    }

    //Creates the part of the expression that will be used later to look for criteria on reviews, for example: room|food|staff
    private function createExpressionPart($criteria)
    {
        $expressionPart = "";

        foreach ($criteria as $index => $crit) {
            $expressionPart .= $crit['name'];
            if ($index < count($criteria) -1) {
                $expressionPart .= "|";
            }
        }

        return $expressionPart;
    }

    //Create the text to be shown on the grid
    private function foundToString($analysis)
    {
        $foundString = "";
        foreach ($analysis as $line) {

            $foundString .= $line['line']."  :  ".$line['points']." <br>";
        }

        return $foundString;
    }

    //Calculates the current score for the review based on the analysis
    private function currentScore($analysis)
    {
        $score = 0;
        foreach ($analysis as $line) {

            $score = $score + $line['points'];
        }

        return $score;
    }

}